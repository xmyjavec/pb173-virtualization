use clap::Parser;
use libxml::xpath::Context;
use std::thread::sleep;
use std::time::Duration;
use virt::connect::Connect;
use virt::domain::Domain;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Connection uri
    #[arg(short = 'c', long = "connect", value_name = "URI")]
    uri: Option<String>,

    /// The domain name
    #[arg(short, long, value_name = "NAME")]
    domain: String,

    /// The domain's interface, either target name or MAC address
    #[arg(short, long, value_name = "IFACE", required_unless_present("list"))]
    interface: Option<String>,

    /// How many seconds sleep between prints
    #[arg(short, long, value_name = "SECONDS")]
    sleep: Option<u64>,

    /// List domain interfaces and exit
    #[arg(short, long)]
    list: bool,
}

fn list_dom_ifaces(dom: &Domain) -> Result<(), virt::error::Error> {
    let domxml = dom.get_xml_desc(0)?;
    let parser = libxml::parser::Parser::default();
    let domxml_doc = parser.parse_string(domxml).unwrap();
    let ctxt = Context::new(&domxml_doc).unwrap();
    let result = ctxt.evaluate("/domain/devices/interface").unwrap();

    for node in &result.get_nodes_as_vec() {
        let typ = node.get_attribute("type");
        let target = node
            .findnodes("./target/@dev")
            .unwrap_or_default()
            .first()
            .map(|x| x.get_content());
        let mac = node
            .findnodes("./mac/@address")
            .unwrap_or_default()
            .first()
            .map(|x| x.get_content());

        println!(
            "type={}\ttarget={}\tmac={}",
            typ.unwrap_or_default(),
            target.unwrap_or_default(),
            mac.unwrap_or_default()
        );
    }

    Ok(())
}

fn main() -> Result<(), virt::error::Error> {
    let cli = Args::parse();
    let mut conn = Connect::open(cli.uri.as_deref())?;
    let dom = Domain::lookup_by_name(&conn, &cli.domain)?;

    if cli.list {
        list_dom_ifaces(&dom)?;
        conn.close()?;
        return Ok(());
    }

    let iface = cli.interface.unwrap();
    let mut stats_old = Domain::interface_stats(&dom, &iface)?;
    loop {
        let how_long: u64 = cli.sleep.unwrap_or(2) * 1000;
        let divisor: i64 = how_long as i64;

        sleep(Duration::from_millis(how_long));
        let stats_new = Domain::interface_stats(&dom, &iface)?;
        println!(
            "RX: {} KiB/s\t\tTX: {} KiB/s\r",
            (stats_new.rx_bytes - stats_old.rx_bytes) / divisor,
            (stats_new.tx_bytes - stats_old.tx_bytes) / divisor
        );

        stats_old = stats_new;
    }
}
