bits 16

start:
  mov dx, 0x3f8     ; What port to output data to (COM1)
  mov si, msg
  mov cl, msg_len
  cld
loop:
  lodsb             ; load byte from DS:SI registers
  out dx, al        ; write byte to serial line
  dec cl
  jnz loop
end:
  hlt

msg:      db 'Hello world!', 0x0a
msg_len:  equ $ - msg
